package com.twuc.webApp.web;


import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.contract.GetProductResponse;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductController {


    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @PostMapping("/products")
    ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest createProductRequest){
        Product newProduct = productRepository.save(new Product(createProductRequest.getName(), createProductRequest.getPrice(), createProductRequest.getUnit()));
        return ResponseEntity.status(201).location(URI.create(String.format("http://localhost/api/products/%d", newProduct.getId()))).build();
    }



    @GetMapping("/products/{productId}")
    ResponseEntity queryProduct(@PathVariable Long productId){
        Optional<Product> optional = productRepository.findById(productId);
        Product product = optional.orElse(null);
        if (product == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(200).body(new GetProductResponse(product));
    }
}
